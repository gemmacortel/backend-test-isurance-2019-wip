<?php

require_once __DIR__  . '/StringTools.php';
/**
 * Develop a Unit Test for StringTools class
 * Try to use PHPUnit best practices (setUp, tearDown, dataProviders, annotations, ...)
 */
class StringToolsTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var StringTools
     */
    private $stringTools;

    public function setUp(): void
    {
        $this->stringTools = new StringTools();
    }
    public function testCapitalize(): void
    {
        $expected = 'bananas';
        $actual = $this->stringTools->capitalize('BANANAS');

        $this->assertEquals($expected, $actual);
    }

    public function testCapitalize_NumericInput(): void
    {
        $this->stringTools->capitalize(123);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Capitalize only accepts strings!');
    }

    public function testCapitalize_NotScalar(): void
    {
        $this->stringTools->capitalize(123);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Capitalize only accepts strings!');
    }
}
