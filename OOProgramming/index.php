<?php

use Parser\FeedParser;

require_once 'autoload.php';

/**
 * You can start writing code here. Please do not change any skeleton classes.
 * You can create as many files and folders and namespaces as you need
 *
 * You might notice that we are using Symfony2 UniversalClassLoader. Do not worry if you have
 * never used Symfony2, if you follow PSR-0 it will just work as expected.
 */

$parser = new FeedParser('/Users/gemma/Code/backend-test-isurance-2019/OOProgramming/products.xml');
try {
    $parser->parse();
} catch (Exception $e) {
    die();
}
