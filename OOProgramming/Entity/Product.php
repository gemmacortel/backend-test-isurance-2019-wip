<?php

namespace Entity;

use DateTime;

class Product
{
    private $id;

    private $title;

    private $link;

    private $pubDate;

    public function __construct(string $title, string $link, DateTime $pubDate)
    {
        $this->id = rand(1, 1111);
        $this->title = $title;
        $this->link = $link;
        $this->pubDate = $pubDate;
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     * @return $this
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPubDate()
    {
        return $this->pubDate;
    }

    /**
     * @param mixed $pubDate
     * @return $this
     */
    public function setPubDate($pubDate)
    {
        $this->pubDate = $pubDate;
        return $this;
    }


}
